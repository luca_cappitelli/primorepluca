package controller;

import java.sql.Connection;
import java.sql.DriverManager;
import java.util.ArrayList;

import dao.ConnectionManager;
import dao.DAOFILM;
import dao.DAOFilmImpl;
import model.Film;

public class Main {

	public static void main(String[] args) {		
	
	DAOFILM daoFilm= DAOFilmImpl.getInstance();
	
	//creazione
	Film film= new Film(1, "Titsnic", "Drammatico");
	Film film2= new Film(2, "IT", "Horror");
	Film film3= new Film("Hancock", "Azione");
//	daoFilm.createFilm(film);
//	daoFilm.createFilm(film2);
//	daoFilm.createFilm(film3);
	
	
	//letturasingolo
	System.out.println("Lettura di una persona");
	Film lettura=daoFilm.readFilm(1);
	System.out.println(lettura);
	Film lettura1= daoFilm.readFilm(2);
	System.out.println(lettura1);
	Film lettura2= daoFilm.readFilm(3);
	System.out.println(lettura2);
	
	//aggiornamento
	film.setTitolo("Titanic");
	daoFilm.updateFilm(film);
	
	//eliminazione
	film3.setId(5);
	daoFilm.deleteFilm(film3);
	
	//lettura
	System.out.println("Read all");
	
	System.out.println("Fine operazione");	
	
	
	
	}

}
