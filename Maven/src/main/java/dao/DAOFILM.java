package dao;

import java.util.List;

import model.Film;

public interface DAOFILM{

	void createFilm(Film film);
	 
	Film readFilm(int id);
	
	void updateFilm(Film film);
	
	void deleteFilm(Film film);
	
	List<Film> readAll();
	
	
	
	
}
