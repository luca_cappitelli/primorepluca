package dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import model.Film;

public class DAOFilmImpl implements DAOFILM {
	
	//esempio corretto di uso del singleton 
	
	private static DAOFilmImpl instance= null;
	
	private DAOFilmImpl() {
		// TODO Auto-generated constructor stub
	}
	
	public static DAOFilmImpl getInstance() {
		if(instance==null) {
			instance=new DAOFilmImpl();
		}
		return instance;
	}
	
	
	
	
	
	
	

	@Override
	public void createFilm(Film film) {

		String sql = "INSERT INTO film (id, titolo, genere) VALUES (?, ?, ?)";

		try (PreparedStatement stm = ConnectionManager.getConnection().prepareStatement(sql);) {

			stm.setInt(1, film.getId());
			stm.setString(2, film.getTitolo());
			stm.setString(3, film.getGenere());
			

			stm.execute();

		} catch (SQLException e) {
			e.printStackTrace();
		}



	}

	@Override
	public Film readFilm(int id) {

		String sql = "SELECT * FROM film WHERE id=?";
		Film film= null;

		try (PreparedStatement stm = ConnectionManager.getConnection().prepareStatement(sql);) {

			film = new Film();
			stm.setInt(1, id);
			ResultSet result = stm.executeQuery();

			while (result.next()) {

				film.setId(result.getInt("id"));
				film.setTitolo(result.getString("titolo"));
				film.setGenere(result.getString("genere"));
			

			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return film;
	}

	@Override
	public void updateFilm(Film film) {

		String sql = "UPDATE film SET titolo=?, genere=? WHERE id=?";

		try (PreparedStatement stm = ConnectionManager.getConnection().prepareStatement(sql);) {

			stm.setString(1, film.getTitolo());
			stm.setString(2, film.getGenere());
			stm.setInt(3, film.getId());

			stm.execute();

		} catch (SQLException e) {
			e.printStackTrace();
		}


	}

	@Override
	public void deleteFilm(Film film) {
	
		String sql= "DELETE FROM film where id=?";
		try (PreparedStatement stm = ConnectionManager.getConnection().prepareStatement(sql);){
			
			stm.setInt(1, film.getId());
			stm.execute();
			
			}catch (SQLException e) {
				e.printStackTrace();
			}
		
		
		
		
	}

	@Override
	public List<Film> readAll() {

		String sql = "SELECT * FROM persona";
		List<Film> listaFilm= null;
		try(PreparedStatement stm = ConnectionManager.getConnection().prepareStatement(sql);
				ResultSet result= stm.executeQuery(sql)) {
			
			listaFilm= new ArrayList<>();
			while(result.next()) {
				Film film = new Film();
				
				film.setId(result.getInt("id"));
				film.setTitolo(result.getString("titolo"));
				film.setGenere(result.getString("genere"));
				
				listaFilm.add(film);				
				
			}
			
			
		} catch (SQLException e) {
		e.printStackTrace();
		}
		
		
	
		
		
		
		
		return listaFilm;
	}

}

