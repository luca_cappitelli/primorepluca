package model;

public class Film {
private int id;
private String titolo;
private String genere;

public Film() {
	// TODO Auto-generated constructor stub
}


public Film(int id, String titolo, String genere) {
	super();
	this.id = id;
	this.titolo = titolo;
	this.genere = genere;
}

public Film(String titolo,String genere) {
	this.titolo= titolo;
	this.genere= genere;
}



public int getId() {
	return id;
}

public void setId(int id) {
	this.id = id;
}

public String getTitolo() {
	return titolo;
}

public void setTitolo(String genere) {
	this.titolo = genere;
}

public String getGenere() {
	return genere;
}

public void setGenere(String genere) {
	this.genere = genere;
}

@Override
public String toString() {
	return "Film [id=" + id + ", titolo=" + titolo + ", genere=" + genere + "]";
}






}
